jQuery(document).ready(function($) {
	$( "#dialog" ).dialog({
      autoOpen: false,
      show: {
        effect: "blind",
        duration: 1000
      },
      hide: {
        effect: "explode",
        duration: 1000
      }
    });

$('.animal-button').on('click', function(event) {
	event.preventDefault();
	var targetAnimal = $(this).attr('target');

	$('.modal-wrapper').find('h1').html(targetAnimal.toUpperCase()+' FACTS');

	$( "#dialog" ).dialog( "open" );

	$(this).addClass('is-show');
});




});

 
